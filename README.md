# my_cv

docker run --rm -it -v "c:\git\cv":/workdir danteev/texlive latexmk -pdf main.tex


[CV PL](https://pirgos.gitlab.io/cv_pol/Szymon_Rogalski_pol.pdf)

[CV EN](https://pirgos.gitlab.io/cv_pol/Szymon_Rogalski_eng.pdf)
